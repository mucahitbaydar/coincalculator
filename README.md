# CoinCalculator

This project is developed for part of the job hiring process.

#Usage

You can clone the project with following command
```
git clone https://mucahitbaydar@bitbucket.org/mucahitbaydar/coincalculator.git
```

Then you can use maven to build the project with following command
```
mvn clean install
```

After the runnable jar file created you can try following commands
```
java -jar coinCalculator-0.0.1-SNAPSHOT.jar 5 10 20 20 25 50
java -jar coinCalculator-0.0.1-SNAPSHOT.jar 5
java -jar coinCalculator-0.0.1-SNAPSHOT.jar
```

Input format is shown above, program is waiting for space seperated integers
As an assumption last given value is always considered as price
If there is no input, program will execute with no coins and 0 price
If there is only one input, program will assume it is the price and there is no coin presented