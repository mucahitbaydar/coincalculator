package com.baydar.main;
//Mucahit Baydar
//03.09.2018
import java.util.ArrayList;
import java.util.Collections;

public class CoinCalculator {

	public static void main(String[] args) {
		//If there is no argument call method with null and 0
		if (args.length == 0) {
			System.out.println(minCoins(null, 0));
		} else if (args.length == 1) {
			//If there is only one argument assume it is price
			if (isPositiveInteger(args[0])) {
				//Check for any undesirable input
				System.out.println(minCoins(null, Integer.parseInt(args[0])));
			} else {
				System.out.println("Please enter positive integer value(s)");
				System.out.println("Your given value is : " + args[0]);
			}
		} else {
			//If there is more than one arguments assume the last one as a price
			for (int i = 0; i < args.length; i++) {
				//Check for any undesirable input
				if (!isPositiveInteger(args[i])) {
					System.out.println("Please enter positive integer value(s)");
					System.out.println("Your given value is : " + args[i]);
					System.exit(0);
				}
			}
			
			//Add coins to arraylist
			ArrayList<Integer> coins = new ArrayList<Integer>();
			for (int i = 0; i < args.length - 1; i++) {
				coins.add(Integer.parseInt(args[i]));
			}
			
			//Calculate and Print result
			System.out.println(minCoins(coins,Integer.parseInt(args[args.length-1])));
		}
	}

	// Calculates minimum number of coins required to change the Price
	public static ArrayList<Integer> minCoins(ArrayList<Integer> coins, int Price) {
		//If there is no coin return empty list
		if(coins == null) {
			return new ArrayList<Integer>();
		}
		// Sort coins
		Collections.sort(coins);
		// Reverse the sorted array so we have them in descending order
		Collections.reverse(coins);

		// Keeping results for every amount lower than and equal to Price
		ArrayList<ArrayList<Integer>> resultSet = new ArrayList<ArrayList<Integer>>();
		// Create null arraylists
		for (int i = 0; i < Price + 1; i++) {
			resultSet.add(new ArrayList<Integer>());
		}

		// For every amount of price, do the following calculations
		for (int i = 1; i <= Price; i++) {
			// Go through all coins
			for (int j = 0; j < coins.size(); j++) {
				// If coin is smaller than amount
				if (coins.get(j) < i) {
					// Get result for already calculated value
					// eg; if i is 30 and selected coin is 25 we are trying to retrieve result for 5
					@SuppressWarnings("unchecked")
					ArrayList<Integer> current_result = (ArrayList<Integer>) resultSet.get(i - coins.get(j)).clone();
					// If there is no calculated result there,
					// We should continue with the next coin
					if (!current_result.isEmpty()) {
						// If resultSet is empty then there is no calculated value yet,
						// If it is not empty that means we need to find a better result
						if (resultSet.get(i).isEmpty() || current_result.size() + 1 < resultSet.get(i).size()) {
							// We also need to check if selected coin is also appeared in
							// already calculated result
							if (current_result.contains(coins.get(j))) {
								int occurrencesInResult = Collections.frequency(current_result, coins.get(j));
								int occurrencesInCoins = Collections.frequency(coins, coins.get(j));
								// Check if we have at least 1 more selected coin in the pocket
								if (occurrencesInResult + 1 > occurrencesInCoins) {
									continue;
								}
							}
							// Add selected coin to current result
							current_result.add(coins.get(j));
							// Add new calculation to result set
							resultSet.set(i, current_result);
						}
					}
					// If coin is equal to amount, means there is no need for any calculation
				} else if (coins.get(j) == i) {
					// Add selected coin to result set
					ArrayList<Integer> cur_res = new ArrayList<Integer>();
					cur_res.add(coins.get(j));
					resultSet.set(i, cur_res);
					break;
				}
			}
		}
		// Return the calculated result
		return resultSet.get(Price);
	}

	//Check if given string is positive integer
	public static boolean isPositiveInteger(String s) {
		int val = 0;
		try {
			val = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		// check if it is positive 
		if(val>=0) {
			return true;
		}else {
			return false;
		}
	}

}
