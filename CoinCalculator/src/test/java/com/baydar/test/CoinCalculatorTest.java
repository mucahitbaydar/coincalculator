package com.baydar.test;
//Mucahit Baydar
//03.09.2018

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

import com.baydar.main.CoinCalculator;

public class CoinCalculatorTest {
	
	//Given test case
	@Test
	public void testGivenCase() {
		ArrayList<Integer> coins = new ArrayList<Integer>();
		Collections.addAll(coins, 5, 5, 10, 10, 10, 50);
		int Price = 70;
		ArrayList<Integer> expected_result = new ArrayList<Integer>();
		Collections.addAll(expected_result, 10, 10, 50);
		ArrayList<Integer> result = CoinCalculator.minCoins(coins, Price);
		assertEquals(result, expected_result);
	}
	
	//No result found test
	@Test
	public void testCase1() {
		ArrayList<Integer> coins = new ArrayList<Integer>();
		Collections.addAll(coins, 5, 10, 20, 50);
		int Price = 90;
		ArrayList<Integer> expected_result = new ArrayList<Integer>();
		ArrayList<Integer> result = CoinCalculator.minCoins(coins, Price);
		assertEquals(result, expected_result);
	}
	
	//With 1 coin test
	@Test
	public void testCase2() {
		ArrayList<Integer> coins = new ArrayList<Integer>();
		Collections.addAll(coins, 5, 10, 20, 20, 50);
		int Price = 50;
		ArrayList<Integer> expected_result = new ArrayList<Integer>();
		expected_result.add(50);
		ArrayList<Integer> result = CoinCalculator.minCoins(coins, Price);
		assertEquals(result, expected_result);
	}
	
	//Empty coin set test
	@Test
	public void testCase3() {
		ArrayList<Integer> coins = new ArrayList<Integer>();
		int Price = 50;
		ArrayList<Integer> expected_result = new ArrayList<Integer>();
		ArrayList<Integer> result = CoinCalculator.minCoins(coins, Price);
		assertEquals(result, expected_result);
	}
	
	//Empty coin set and price is 0 test
	@Test
	public void testCase4() {
		ArrayList<Integer> coins = new ArrayList<Integer>();
		int Price = 0;
		ArrayList<Integer> expected_result = new ArrayList<Integer>();
		ArrayList<Integer> result = CoinCalculator.minCoins(coins, Price);
		assertEquals(result, expected_result);
	}
	
	//Price is 0 test
	@Test
	public void testCase5() {
		ArrayList<Integer> coins = new ArrayList<Integer>();
		Collections.addAll(coins, 5, 10, 20, 20, 50);
		int Price = 0;
		ArrayList<Integer> expected_result = new ArrayList<Integer>();
		ArrayList<Integer> result = CoinCalculator.minCoins(coins, Price);
		assertEquals(result, expected_result);
	}
	
	//Recursive method fails test
	//When using recursive method to solve this problem, result will be 25,10,5 instead of 20,20
	@Test
	public void testCase6() {
		ArrayList<Integer> coins = new ArrayList<Integer>();
		Collections.addAll(coins, 5, 10, 20, 20, 25);
		int Price = 40;
		ArrayList<Integer> expected_result = new ArrayList<Integer>();
		Collections.addAll(expected_result, 20, 20);
		ArrayList<Integer> result = CoinCalculator.minCoins(coins, Price);
		assertEquals(result, expected_result);
	}
	
	//Test where the end results can vary, but application fails to pass both cases
	//While 5,25,20 pass the test, 10,20,20 will fail 
	@Test
	public void testCase7() {
		ArrayList<Integer> coins = new ArrayList<Integer>();
		Collections.addAll(coins, 5, 10, 20, 20, 25);
		int Price = 50;
		ArrayList<Integer> expected_result = new ArrayList<Integer>();
		Collections.addAll(expected_result, 5, 25, 20);
		ArrayList<Integer> result = CoinCalculator.minCoins(coins, Price);
		assertEquals(result, expected_result);
	}
}
